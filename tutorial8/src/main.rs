fn main() {
    println!("Hello, world!");
    print_sum(20, 30);
    blocks_are_expressions();
    function_with_return_value();
    function_returning_before_last_line();
}

fn print_sum(x: i32, y: i32) {
    println!("The sum is: {}", x + y);
}

// Expressions can be assigned, statements can not.
#[allow(unused_must_use)]
fn blocks_are_expressions() {
    println!("blocks_are_expressions");

    // let x = let y = 3; would throw an error,
    // because assignments are statements and no expressions

    let number = {
        // the block { ... } is an expression and can be assigned
        let x = 3; //  this is a statement
        x + 1 // this is returned, because there is no semicolon at the end
    };

    println!("number is: {}", number);

    let number = {
        // the block { ... } is an expression and can be assigned
        let x = 3; //  this is a statement
        x + 1; // this is not returned, because there is a semicolon
    };

    // {:?} formats the "next" value passed to a formatting macro using std::fmt::Debug
    // the standard formatting is not defined for the object of type ()
    println!("number is: {:?}", number);
}

fn function_with_return_value() {
    println!("function_with_return_value");

    let x = add_numbers(1, 1);
    println!("x is: {}", x);
}

fn add_numbers(x: i32, y: i32) -> i32 {
    // return x + y; does the same
    x + y
}

fn function_returning_before_last_line() {
    println!("function_returning_before_last_line");

    let status = multiple_return_statements(true);
    println!("status is: {}", status);

    let status = multiple_return_statements(false);
    println!("status is: {}", status);
}

fn multiple_return_statements(return_early: bool) -> String {
    if return_early {
        return "Early return".to_string(); // return keyword is mandatory here
    }
    "Late return".to_string() // return keyword is optional here
}
