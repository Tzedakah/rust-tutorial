#!/usr/bin/env sh
SCRIPT_DIRECTORY=$(dirname "$(readlink -f "$0")")
docker build -t rust-tutorial "$SCRIPT_DIRECTORY"