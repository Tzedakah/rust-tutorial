#!/usr/bin/env sh
SCRIPT_DIRECTORY=$(readlink -f "$0")
PROJECT_DIRECTORY=$(dirname "$SCRIPT_DIRECTORY")
docker run -it\
  -v "$PROJECT_DIRECTORY"/..:/rust-tutorial \
  -w /rust-tutorial \
   rust-tutorial $1
