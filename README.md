# Rust-Tutorial

## Requirements

Docker needs to be installed in your environment.

## Run rust commands

This tutorial uses Docker to run (Rust) commands.
If you have never built the docker container of this tutorial,
then do so by running

```shell
$ sh docker/build.sh
```

Now you can use the script `docker/run.sh` to run (Rust) commands
inside a docker container.
Put the command you want to run in quotation marks
and pass it to the script as argument.
All project files are available in the container as you can see by running

```shell
$ sh docker/run.sh 'ls'
```

You also can run Rust commands, e.g.

```shell
$ sh docker/run.sh 'rustc --version'
```

## Tutorial origin

* The tutorials 2 - 8 are based on
[this tutorial by Tech With Tim](https://www.youtube.com/playlist?list=PLzMcBGfZo4-nyLTlSRBvo0zjSnCnqjHYQ).
* The tutorials 9 + 10 are based on the 
  [rust doc](https://doc.rust-lang.org/stable/book/ch07-02-defining-modules-to-control-scope-and-privacy.html)


## Important learnings

### Lesson 2

* Build a project with `cargo build`

### Lesson 3

* Variables declared with no explicit type are implicitly typed
  and the type can never change after the declaration
* Variables are immutable by default.
  Use `mut` to make them mutable
* Variables should only be mutable if that is necessary
* Variables can have the same name.
* Variables with the same name can have different types
* Declaring a variable with the name of an already existing variable
  overshadows the already existing variable (only) inside the scope
  in which the new variables is declared!
* Constants are immutable and can not be implicitly typed.
  Two constants can not have the same name.

### Lesson 5

* `::` is the path separator operator.
    * It can be used to go from a crate to a module, e.g. `std::io`
    * It can be used to go from a module to a function, e.g. `String::new`
* `&mut` is a mutable reference, meaning i.e. a reference to an object
  and the referenced object can be mutated with this reference.
### Lesson 8
* `{:?}` formats the "next" value passed to a formatting macro
  using `std::fmt::Debug`
* `()` is a type with the single value `()`
  * It's single value is returned by default
    to indicate that nothing interesting is returned
  * It's similar to `void` in java