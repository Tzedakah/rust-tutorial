use crate::garden::vegetables::Asparagus;

mod garden;

fn main() {
    println!("Hi, I am an {:?}", Asparagus {});
}
