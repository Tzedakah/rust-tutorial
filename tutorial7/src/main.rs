fn main() {
    conditional_operators();
    conditional_operators_only_can_can_compare_objects_of_same_type();
    logical_operators();
    simple_if_statement();
    simple_if_else_statement();
    simple_if_elseif_statement();
}

fn conditional_operators() {
    println!("conditional_operators");

    let result_1 = 2 < 3;
    let result_2 = 2 <= 3;

    println!("result_1 is: {}", result_1);
    println!("result_2 is: {}", result_2);
}

fn conditional_operators_only_can_can_compare_objects_of_same_type() {
    let _x: f64 = 2.2;
    let _y: i64 = 2;
    // _x > _y; would throw an error, because x and y don't have the same type
}

fn logical_operators() {
    println!("logical_operators");
    let x = !true;
    let y = true && true;
    let z = true || false;

    println!("x is: {}", x);
    println!("y is: {}", y);
    println!("z is: {}", z);
}

fn simple_if_statement() {
    println!("simple_if_statement");

    let status = "SUCCESS";

    if status == "SUCCESS" {
        println!("Congratulations!");
    }
}

fn simple_if_else_statement() {
    println!("simple_if_statement");

    let status = "FAIL";

    if status == "SUCCESS" {
        println!("Congratulations!");
    } else {
        println!("Try again!");
    }
}

fn simple_if_elseif_statement() {
    println!("simple_if_elseif_statement");

    let status = "FAIL";

    if status == "SUCCESS" {
        println!("Congratulations!");
    } else if status == "FAIL" {
        println!("Maybe next time!");
    }

    let status = "UNDEFINED";

    if status == "SUCCESS" {
        println!("Congratulations!");
    } else if status == "FAIL" {
        println!("Maybe next time!");
    }
}
