#[allow(dead_code)]
mod front_of_house {
    mod hosting {
        fn add_to_waiting_list() {}

        fn seat_at_table() {}
    }

    mod serving {
        fn take_order() {}

        fn serve_order() {}

        fn take_payment() {}
    }
}
