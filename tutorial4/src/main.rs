fn main() {
    println!("Hello, world!");
    declare_integers();
    can_not_assign_from_one_integer_type_to_another();
    declare_a_tuple();
    access_a_value_in_a_tuple();
    only_elements_of_mutable_tuples_can_be_reassigned();
    declare_an_array();
    access_elements_of_an_array();
    arrays_have_a_fixed_length();
}

fn declare_integers() {
    let _x: i8 = 6;
    let _x: i16 = 6;
    let _x: i32 = 6;
    let _x: i64 = 6;
    let _x: i128 = 6;
}

fn can_not_assign_from_one_integer_type_to_another() {
    let _x: i8 = 6;
    // let _y: i16 = _x; would throw an error, because the types do not match
}

fn declare_a_tuple() {
    let _tuple: (i32, bool, char) = (1, true, '5');
}

fn access_a_value_in_a_tuple() {
    println!("access_a_value_in_a_tuple");

    let tuple: (i32, bool, char) = (1, true, '5');
    // println!("tuple is: {}", tuple); would throw an error,
    // because tuple can not be formatted with the default formatter
    // Same for arrays

    println!("The first element of tuple is: {}", tuple.0);
    println!("The second element of tuple is: {}", tuple.1);
    println!("The third element of tuple is: {}", tuple.2);
}

fn only_elements_of_mutable_tuples_can_be_reassigned() {
    println!("elements_of_immutable_tuples_can_not_be_reassigned");

    let _tuple = (1, true, '5');
    // _tuple.0 = 2; would throw an error, because tuple is immutable
    // Same for arrays

    let mut tuple = (1, true, '5');
    tuple.0 = 2;
    println!("The first element of tuple is: {}", tuple.0);
}

fn declare_an_array() {
    let _array = [1, 2, 3];
    // let _array = [1, true]; would throw an error, because 1 and true have no common type.
}

fn access_elements_of_an_array() {
    println!("access_elements_of_an_array");
    let array = [1, 2, 3];
    println!("The first element of array is: {}", array[0]);
}

fn arrays_have_a_fixed_length() {
    let mut _array = [1, 2, 3];
    // _array = [2, 3, 4, 5]; would throw an error, because the size is part of the type of an array
}
