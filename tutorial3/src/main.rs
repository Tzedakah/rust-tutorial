fn main() {
    declare_and_print_variable();
    declare_and_print_explicitly_typed_variable();
    variables_are_immutable_by_default();
    declare_and_print_mutable_variable();
    declare_and_print_variables_with_same_name();
    variables_type_can_not_be_changed();
    variables_with_same_name_can_have_different_types();
    variable_overshadows_variable_with_same_name_only_inside_the_scope_it_is_declared_in();
    variable_can_be_used_in_interior_scope();
    declare_and_print_a_constant();
}

fn declare_and_print_variable() {
    println!("declare_and_print_variable");
    let x = 4;
    println!("x is : {}", x);
}

fn declare_and_print_explicitly_typed_variable() {
    println!("declare_and_print_explicitly_typed_variable");
    let x: u32 = 4;
    println!("x is : {}", x);
}

fn variables_are_immutable_by_default() {
    println!("variables_are_immutable_by_default");
    let x = 4;
    println!("x is : {}", x);
    // x = 5; would throw an error, because variables are immutable by default
}

fn declare_and_print_mutable_variable() {
    println!("declare_and_print_mutable_variable");
    let mut x = 4;
    println!("x is : {}", x);
    x = 5;
    println!("x is : {}", x);
}

fn declare_and_print_variables_with_same_name() {
    println!("declare_and_print_variables_with_same_name");
    let x = 4;
    println!("x is : {}", x);
    let x = x + 1;
    println!("x is : {}", x);
}

fn variables_type_can_not_be_changed() {
    println!("variables_type_can_not_be_changed");
    let x = 4;
    println!("x is : {}", x);
    // x = "hello"; would throw an error, because x has type i32
}

fn variables_with_same_name_can_have_different_types() {
    println!("variables_with_same_name_can_have_different_types");
    let x = 4;
    println!("x is : {}", x);
    let x = "hello";
    println!("x is : {}", x);
}

fn variable_overshadows_variable_with_same_name_only_inside_the_scope_it_is_declared_in() {
    println!(
        "variable_overshadows_variable_with_same_name_only_inside_the_scope_it_is_declared_in"
    );
    let x = 4;
    println!("x is : {}", x);

    {
        let x = 2;
        println!("x is : {}", x);
    }

    let x = x + 1;
    println!("x is : {}", x);
}

fn variable_can_be_used_in_interior_scope() {
    println!("variable_can_be_used_in_interior_scope");
    let x = 4;
    println!("x is : {}", x);

    {
        let x = x - 2;
        println!("x is : {}", x);
    }

    let x = x + 1;
    println!("x is : {}", x);
}

fn declare_and_print_a_constant() {
    println!("declare_and_print_a_constant");
    const SECONDS_IN_MINUTES: u32 = 60;
    println!("SECONDS_IN_MINUTES is : {}", SECONDS_IN_MINUTES);

    // SECONDS_IN_MINUTES = 100; would throw an error, because constants are immutable

    // const SECONDS_IN_MINUTES: u32 = 100; would throw an error,
    // because constant names can not be reused
}

