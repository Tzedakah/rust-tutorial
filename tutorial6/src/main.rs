use std::io;

fn main() {
    different_number_types_can_not_be_add();
    numbers_can_only_be_added_if_the_sum_is_in_the_range_of_the_data_type();
    dividing_integers_always_rounds_down();
    use_underscores_to_make_numbers_readable();
    explicit_type_conversion();
    casting_to_a_too_small_type_can_lead_to_hard_to_detect_bugs_without_throwing_errors();
    get_number_from_user();
}

fn different_number_types_can_not_be_add() {
    let _x: u8 = 12; // u8 range: 0 - 255
    let _y: i8 = 10; // i8 range: -128 - 127

    // let z = _x + _y; would throw an error, because both addends must be of the same type
}

fn numbers_can_only_be_added_if_the_sum_is_in_the_range_of_the_data_type() {
    let _x: u8 = 156; // u8 range: 0 - 255
    let _y: u8 = 100; // u8 range: 0 - 255

    // let z = _x + _y; would throw an error, because 256 is out of range for u8
    // same for subtraction, multiplication, division
}

fn dividing_integers_always_rounds_down() {
    println!("dividing_integers_always_rounds_down");
    let x: u8 = 5;
    let y: u8 = 3;

    // let z: f64 = x / y; would throw an error, because dividing two u8 numbers returns a u8 number
    let z = x / y;
    println!("z is: {}", z);
}

fn use_underscores_to_make_numbers_readable() {
    println!("use_underscores_to_make_numbers_readable");
    let x: i64 = 1_000_000;
    println!("x is: {}", x);
}

fn explicit_type_conversion() {
    println!("explicit_type_conversion");
    let x = 10 as f64;
    let y = 2 as i64;

    let z = x / (y as f64);
    println!("z is: {}", z);
}

fn casting_to_a_too_small_type_can_lead_to_hard_to_detect_bugs_without_throwing_errors() {
    println!("casting_to_a_too_small_type_can_lead_to_hard_to_detect_bugs_without_throwing_errors");
    let x: u16 = 256;
    let y: u8 = 2;

    let z = (x as u8) + y;
    println!("z is: {}", z);
}

fn get_number_from_user() {
    println!("get_number_from_user");
    println!("Enter an integer and submit with Enter!");
    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Failed to read user input!");

    let int_input: i64 = input.trim().parse().unwrap();

    println!("The successor is: {}", int_input + 1);
}
