use std::io;

fn main() {
    let mut input = String::new();
    println!("Type something and press Enter!");
    io::stdin()
        .read_line(&mut input)
        .expect("Failed to read line!");
    println!("The input was: {}", input);
    println!("Program finished!")
}
